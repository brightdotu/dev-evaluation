// Parent component of SearchBar and PeopleList
import React, { useState, useEffect } from 'react';
import SearchBar from './SearchBar';
import PeopleList from './PeopleList';

const SearchPage = (props) => {
  const [input, setInput] = useState('');
  const [peopleListDefault, setPeopleListDefault] = useState();
  const [peopleList, setPeopleList] = useState();
  // const results = [];

  // Fetches all people
  const fetchJson = async () => {
    const results = [];
    let url = 'https://swapi.dev/api/people/';
    do {
      const response = await fetch(url);
      const data = await response.json();
      url = data.next;
      results.push(...data.results);
    } while (url)
    setPeopleList(results);
    setPeopleListDefault(results);

    return results;

    // return await fetch(url)
    //   .then(response => response.json())
    //   .then(data => {
    //     setPeopleList(data.results)
    //     setPeopleListDefault(data.results)
    //   });
    // // console.log(fetchJson)
  }

  // Handles keyword change and filtering the people list
  const updateInput = async (input) => {
    const filtered = peopleListDefault.filter(person => {
      return person.name.toLowerCase().includes(input.toLowerCase())
    })
    setInput(input);
    setPeopleList(filtered);
  }

  // Initiate fetchJson
  useEffect( () => {fetchJson()}, []);

  // Passes the input & updateInput variables as props to the SearchBar component
  // Passes the peopleList variable as props to the PeopleList component
  return (
    <>
      <h1>People of the Star Wars Universe</h1>
      <i><p><small>(Click on name for more details.)</small></p></i>
      <SearchBar
        input={input}
        onChange={updateInput}
      />
      <PeopleList peopleList={peopleList} />
    </>
  );
}

export default SearchPage

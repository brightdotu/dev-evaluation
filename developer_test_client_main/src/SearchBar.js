// This component is a basic input bar with some styling.
import React from 'react';

const SearchBar = ({input:keyword, onChange:setKeyword}) => {
  const SearchBarStyling = {width:"20rem", background:"#F2F1F9", border:"none", padding:"0.5rem"};
  return(
    <input
      style = {SearchBarStyling}
      key = "randKey"
      value = {keyword}
      placeholder = {"find person"}
      onChange = {(e) => setKeyword(e.target.value)}
    />
  );
}


export default SearchBar

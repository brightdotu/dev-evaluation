// This component shows the list of people in the star wars universe.
import React from 'react';

const PeopleList = ({peopleList=[]}) => {
  const DataStyling = {background:"inherit", color:"inherit", border:"none", padding:"inherit", cursor:"pointer"}
  return (
    peopleList.map((data,index) => {
        if (data) {

          const getSpecies = async() => {
            if (data.species.length === 0){
              console.log("Specie: ", "Human")
              return "Human"
            } else {
              const response = await fetch(data.species)
              const specie = await response.json()
              console.log("Specie: ", specie.name);
              return specie.name
            }
          }

          const getPlanet = async () => {
            const response = await fetch(data.homeworld)
            const planet = await response.json()
            // this.state = response.name;
            console.log("Planet: ", planet.name);
            return planet.name
          }

          const getFilms = async () => {
            let urls = data.films;
            const films = [];
            for (const url of urls) {
              const response = await fetch(url)
              const film = await response.json()
              films.push(film.title);
            }
            console.log("Films: ", films)
            return films;
          }

          return (
            <div key={data.name}>
              <button style={DataStyling} onClick={() => {alert(` Name: ${data.name} \n Gender: ${data.gender} \n Species: ${getSpecies()} \n Homeworld ${getPlanet()} \n Films: ${getFilms()}`)}}>
                <h2>{data.name}</h2>
              </button>
            </div>
          )
        }
        return null
      })
  );
}


export default PeopleList

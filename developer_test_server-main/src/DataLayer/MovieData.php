<?php
namespace DataLayer;

class MovieData
{
	private \PDO $db;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}

	public function getAllMovies(): array
	{
		$stmt = $this->db->prepare("select * from film");
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//Search movie by title
	public function searchMovieByTitle(): array
	{
		$stmt = $this->db->prepare("SELECT * FROM film_list WHERE title =:film_title");
		$stmt->bindParam(":film_title", $film_title);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	// Filter movies by rating
	public function filterByRating(): array
	{
		$stmt = $this->db->prepare("SELECT * FROM film_list WHERE rating = :film_rating");
		$stmt->bindParam(":film_rating", $film_rating);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	// Filter movies by category
	public function filterByCategory(): array
	{
		$stmt = $this->db->prepare("SELECT title FROM film INNER JOIN film_category USING (film_id) INNER JOIN category USING (category_id) WHERE name = :film_category");
		$stmt->bindParam(":film_category", $film_category);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	// Create a new movie
	public function addNewMovie()
	{
		$stmt = $this->db->prepare("INSERT INTO film (title, description, release_year, length, rating) VALUES (:title, :description, :release_year, :length, :rating)");
		$stmt->bindParam(":title", $title);
		$stmt->bindParam(":description", $description);
		$stmt->bindParam(":release_year", $release_year);
		$stmt->bindParam("length", $length);
		$stmt->bindParam(":rating", $rating);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}

<?php
namespace Controllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use \DataLayer\MovieData;

class MovieController
{
	private MovieData $movieData;

	public function __construct(ContainerInterface $container)
	{
		$this->movieData = $container->get('movieData');
	}

	public function listAll(Request $request, Response $response, $args)
	{
		$movies = $this->movieData->getAllMovies();
		return $response->withJson($movies);
	}

	// Search movie by title
	public function listMovieByTitle(Request $request, Response $response, array $args)
	{
		$movieFromTitle = $this->movieData->searchMovieByTitle();
		return $response->withJson($movieFromTitle);
	}

	// Filter movies by rating
	public function listMoviesByRating(Request $request, Response $response, array $args)
	{
		$moviesByRating = $this->movieData->filterByRating();
		return $response->withJson($moviesByRating);
	}

	// Filter movies by category
	public function listMoviesByCategory(Request $request, Response $response, array $args)
	{
		$moviesByCategory = $this->movieData->filterByCategory();
		return $response->withJson($moviesByCategory);
	}

	// Handle bad routes
	public function catchBadRoute(Request $request, Response $response, array $args)
	{
		echo "Bad Route. Route Does Not Exist.";
	}

	// Create a movie
	public function createNewMovie(Request $request, Response $response, aray $args)
	{
		//Handle post request
	}
}

<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require './vendor/autoload.php';

$app = new \Slim\App;
$container = $app->getContainer();

/**
 * Container Definitions
 */

$container['db'] = function($c) {
	$database = $user = $password = "sakila";
	$host = "mysql";

	$db = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $db;
};

$container['movieData'] = function($c) {
	return new \DataLayer\MovieData($c['db']);
};

/**
 * Routes
 */

// List all movies
$app->get('/movies', \Controllers\MovieController::class . ':listAll');

// Search movie by title
$app->get('/movies/search/{film_title}', \Controllers\MovieController::class . ':listMovieByTitle');

// Filter movies by rating
$app->get('/movies/filter/{film_rating}', \Controllers\MovieController::class . ':listMoviesByRating');

// Filter movies by category
$app->get('/movies/filter/{film_category}', \Controllers\MovieController::class . ':listMoviesByCategory');

// Capture bad routes
$app->get('/[{path:.*}]', \Controllers\MovieController::class . ':catchBadRoute');

// Create movie
$app->post('/movie', \Controllers\MovieController::class . ':createNewMovie');

$app->run();

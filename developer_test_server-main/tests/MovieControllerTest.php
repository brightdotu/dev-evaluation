<?php
require_once__DIR__. '/../src/vendor/autoload.php';

use Slim\Environment;
use PHPUnit\Framework\TestCase;

final class MovieControllerTest extends TestCase
{
	// public function testThings(): void
	// {
	// 	$this->assertEquals(true, false);
	// }

	public function request($method, $path, $options = array())
	{
		ob_start();
		Environment::mock(array_merge(array(
			'REQUEST_METHOD' => $method,
			'PATH_INFO' => $path,
			'SERVER_NAME' => 'slim-test.dev',
		), $options));
		$app = new \Slim\Slim();
		$this->app = $app;
		$this->request = $app->request();
		$this->response = $app->response();
		return ob_get_clean();
	}

	public function get($path, $options = array())
	{
		$this->request('GET', $path, $options);
	}

	public function testlistAll() {
		$response = $this->get('/movies');
		$this->assertEquals('200', $this->response->status());
	}

	public function testlistMoviesByTitle(){
		// Test goes here
	}

	public function testlistMoviesByRating() {
		// Test goes on
	}

}
